package com.tuzes.reservation.Base;

import com.tuzes.reservation.Base.service.BaseQueryService;

/**
 * todo
 * service基础类
 * 实现增删改查基本方法
 * 实现此类即可立即拥有一下方法
 */
public abstract class SuperService<T,BaseMapper,QueryVo,ResultVO,SaveVO,UpdateVo>
        implements BaseQueryService<T,BaseMapper,QueryVo,ResultVO,SaveVO,UpdateVo> {

}
