package com.tuzes.reservation.server;

import com.baomidou.mybatisplus.core.exceptions.MybatisPlusException;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.rules.DateType;

import java.util.Scanner;

/**
 * @Description: TODO
 * @author: tuzes
 * @date: Y E A R 年 {YEAR}年YEAR年{MONTH}月09日 11:18
 */

public class CodeGenerator {


    public static String scanner(String tip) {
        Scanner scanner = new Scanner(System.in);
        StringBuilder help = new StringBuilder();
        help.append("请输入" + tip + "：");
        System.out.println(help.toString());
        if (scanner.hasNext()) {
            String ipt = scanner.next();
            if (ipt != null && (!ipt.equals(""))) {
                return ipt;
            }
        }
        throw new MybatisPlusException("请输入正确的" + tip + "！");
    }


    public static void main(String[] args) {
        String url = "jdbc:mysql://localhost:3306/reservation?useUnicode=true&characterEncoding=utf-8&serverTimezone=GMT%2B8";
        String username = "tuzes";
        String password = "368910";
        // 全局配置
        String projectPath = System.getProperty("user.dir");
        GlobalConfig.Builder builder = new GlobalConfig.Builder();
        builder.outputDir(projectPath + "/src/main/java");//设置代码生成路径
        builder.author("tuzes").dateType(DateType.ONLY_DATE).enableSwagger().build();
        GlobalConfig gc = builder.build();
        // 数据源配置
        DataSourceConfig.Builder dscb = new DataSourceConfig.Builder(url, username, password);
        DataSourceConfig dsc = dscb.build();

        // 包配置
        PackageConfig.Builder pcb = new PackageConfig.Builder();
        pcb.parent("com.tuzes.reservation").mapper("biz.mapper").xml("biz.mapper.xml")
                .entity("entity.entity").service("biz.service").serviceImpl("biz.service.impl")
                .controller("controller");
        PackageConfig pc = pcb.build();
        // 策略配置
        StrategyConfig.Builder scb = new StrategyConfig.Builder();
        //要生成的表名配置
        scb.addInclude("reservation");
        scb.addInclude("time_span");
        scb.addInclude("user");
        StrategyConfig sc = scb.build();

        // 代码生成器
        AutoGenerator mpg = new AutoGenerator(dsc);
        mpg.global(gc).packageInfo(pc).strategy(sc);

        // 生成代码
        mpg.execute();
    }

}