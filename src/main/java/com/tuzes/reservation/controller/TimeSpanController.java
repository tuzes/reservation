package com.tuzes.reservation.controller;

import com.tuzes.reservation.biz.service.ITimeSpanService;
import com.tuzes.reservation.entity.entity.TestEntity;
import com.tuzes.reservation.entity.entity.TimeSpan;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  时间段控制器
 * </p>
 *
 *
 * @author tuzes
 * @since 2023-07-09
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("/timeSpan")
@Api(value = "时间段", tags = "时间段")
public class TimeSpanController {
    private final ITimeSpanService timeSpanService;


    @GetMapping("/list")
    @ApiOperation(value = "查询所有时间段", notes = "查询所有时间段")
    public List<TimeSpan> MybatisMapperTest(){
        return timeSpanService.list();
    }

}
