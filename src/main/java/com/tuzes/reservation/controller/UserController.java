package com.tuzes.reservation.controller;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.tuzes.reservation.biz.common.UserType;
import com.tuzes.reservation.biz.service.IUserService;
import com.tuzes.reservation.entity.entity.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  用户控制器
 * </p>
 *
 * @author tuzes
 * @since 2023-07-09
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("/user")
@Api(value = "用户", tags = "用户")
public class UserController {
    private final IUserService userService;

    @PostMapping("/signUp")
    @ApiOperation(value = "注册（普通用户）", notes = "注册（普通用户）")
    public Boolean signUp(@RequestBody User user){
        //手机号作为账号
        user.setAccount(user.getPhone());
        //时间戳作为id
        user.setId(System.currentTimeMillis());
        user.setUserType(UserType.USER);
        if (check(user.getPhone())){
            return userService.save(user);
        }else {
            return false;
        }
    }
    @PostMapping("/addAdmin")
    @ApiOperation(value = "注册（管理员）", notes = "注册（管理员）")
    public Boolean addAdmin(@RequestBody User user){
        //手机号作为账号
        user.setAccount(user.getPhone());
        //时间戳作为id
        user.setId(System.currentTimeMillis());
        user.setUserType(UserType.ADMIN);
        if (check(user.getPhone())){
            return userService.save(user);
        }else {
            return false;
        }
    }

    @GetMapping("/list")
    @ApiOperation(value = "查询所有用户", notes = "查询所有用户")
    public List<User> list(){
      return userService.list();
    }
    @PostMapping("/logIn")
    @ApiOperation(value = "登录", notes = "登录")
    public Boolean logIn(@RequestBody User user){
        userService.getOne(Wrappers.<User>query().eq("account", user.getAccount()).eq("password",user.getPassword()));
        return BeanUtil.isEmpty(user);
    }



    //检查此手机号（账号）是否被注册
    public Boolean check(String phone){
        User user = userService.getOne(Wrappers.<User>query().eq("phone", phone));
        return BeanUtil.isEmpty(user);
    }

}
