package com.tuzes.reservation.controller;

import com.tuzes.reservation.biz.service.TestService;
import com.tuzes.reservation.entity.entity.TestEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/TestController")
@Api(value = "测试控制器", tags = "测试控制器")
public class TestController {
    private final TestService testService;

    @GetMapping("/test")
    @ApiOperation(value = "Mybatis注解Select测试接口", notes = "测试接口")
    public List<TestEntity> test(){
        return testService.selectTest();
    }


    @GetMapping("/MybatisMapperTest")
    @ApiOperation(value = "MybatisMapper文件sql测试接口", notes = "测试接口")
    public List<TestEntity> MybatisMapperTest(){
        return testService.MybatisMapperTest();
    }

    @GetMapping("/MybatisPlusTest")
    @ApiOperation(value = "MybatisPlusTest测试接口", notes = "测试接口")
    public List<TestEntity> MybatisPlusTest(){
        return testService.MybatisPlusTest();
    }
}
