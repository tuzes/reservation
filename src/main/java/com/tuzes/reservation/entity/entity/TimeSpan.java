package com.tuzes.reservation.entity.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 
 * </p>
 *
 * @author tuzes
 * @since 2023-07-09
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@TableName("time_span")
@ApiModel(value = "TimeSpan对象", description = "")
public class TimeSpan implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    @TableField("time_span")
    @ApiModelProperty("时间段")
    private String timeSpan;

    @ApiModelProperty("code（唯一标识")
    private String code;

    @TableField("relation_code")
    @ApiModelProperty("关联时间段codeList（即该此时间段选中后，其他也不能选中的时间段）	")
    private String relationCode;
}
