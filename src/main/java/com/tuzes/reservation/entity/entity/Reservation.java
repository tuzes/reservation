package com.tuzes.reservation.entity.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 
 * </p>
 *
 * @author tuzes
 * @since 2023-07-09
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ApiModel(value = "Reservation对象", description = "")
@TableName("reservation")
public class Reservation implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    @TableField("user_id")
    @ApiModelProperty("用户id")
    private Long userId;

    @ApiModelProperty("电话")
    private String phone;

    @ApiModelProperty("邮箱")
    private String email;

    @ApiModelProperty("姓名")
    private String name;

    @ApiModelProperty("内容")
    private String content;

    @ApiModelProperty("日期")
    private LocalDateTime date;

    @ApiModelProperty("时间段codeList")
    private String timeSpan;

    @ApiModelProperty("预约code")
    private String code;

    @ApiModelProperty("状态")
    private Integer state;

    @TableField("created_time")
    @ApiModelProperty("创建时间")
    private LocalDateTime createdTime;
}
