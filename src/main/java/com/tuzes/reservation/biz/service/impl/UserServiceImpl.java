package com.tuzes.reservation.biz.service.impl;

import com.tuzes.reservation.entity.entity.User;
import com.tuzes.reservation.biz.mapper.UserMapper;
import com.tuzes.reservation.biz.service.IUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author tuzes
 * @since 2023-07-09
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

}
