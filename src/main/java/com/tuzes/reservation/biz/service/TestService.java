package com.tuzes.reservation.biz.service;

import com.tuzes.reservation.entity.entity.TestEntity;

import java.util.List;

/**
 * @description: 测试服务
 * @author: SunYiMing
 * @since: 2023-07-08
 **/

public interface TestService {

    List<TestEntity> selectTest();

    List<TestEntity> MybatisMapperTest();

    List<TestEntity> MybatisPlusTest();
}