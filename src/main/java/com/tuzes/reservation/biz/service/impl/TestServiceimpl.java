package com.tuzes.reservation.biz.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.tuzes.reservation.biz.mapper.TestMapper;
import com.tuzes.reservation.biz.service.TestService;
import com.tuzes.reservation.entity.entity.TestEntity;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @description: 测试服务实现类
 * @author: SunYiMing
 * @since: 2023-07-08
 **/
@Slf4j
@Service
@RequiredArgsConstructor
public class TestServiceimpl implements TestService {
    private final TestMapper testMapper;

    @Override
    public List<TestEntity> selectTest() {
        return testMapper.testSelect();
    }

    @Override
    public List<TestEntity> MybatisMapperTest() {
        return testMapper.MybatisMapperTest();
    }

    @Override
    public List<TestEntity> MybatisPlusTest() {
        return testMapper.selectList(Wrappers.<TestEntity>query().eq("id",1));
    }
}