package com.tuzes.reservation.biz.service.impl;

import com.tuzes.reservation.entity.entity.Reservation;
import com.tuzes.reservation.biz.mapper.ReservationMapper;
import com.tuzes.reservation.biz.service.IReservationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author tuzes
 * @since 2023-07-09
 */
@Service
public class ReservationServiceImpl extends ServiceImpl<ReservationMapper, Reservation> implements IReservationService {

}
