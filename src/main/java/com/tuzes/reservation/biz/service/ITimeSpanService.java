package com.tuzes.reservation.biz.service;

import com.tuzes.reservation.entity.entity.TimeSpan;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tuzes
 * @since 2023-07-09
 */
public interface ITimeSpanService extends IService<TimeSpan> {

    /**
    ·* 通过code获取TimeSpan
    ·*/
    TimeSpan getByCode(String code);
}
