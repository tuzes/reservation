package com.tuzes.reservation.biz.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.tuzes.reservation.entity.entity.TimeSpan;
import com.tuzes.reservation.biz.mapper.TimeSpanMapper;
import com.tuzes.reservation.biz.service.ITimeSpanService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author tuzes
 * @since 2023-07-09
 */
@Service
public class TimeSpanServiceImpl extends ServiceImpl<TimeSpanMapper, TimeSpan> implements ITimeSpanService {

    @Override
    public TimeSpan getByCode(String code) {
        return getOne(Wrappers.<TimeSpan>query().eq("code",code));
    }
}
