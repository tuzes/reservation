package com.tuzes.reservation.biz.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tuzes.reservation.entity.entity.TestEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface TestMapper extends BaseMapper<TestEntity> {
    @Select("select * from test")
    List<TestEntity> testSelect();

    List<TestEntity> MybatisMapperTest();
}
