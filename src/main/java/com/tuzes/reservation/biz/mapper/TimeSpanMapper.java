package com.tuzes.reservation.biz.mapper;

import com.tuzes.reservation.entity.entity.TimeSpan;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author tuzes
 * @since 2023-07-09
 */
public interface TimeSpanMapper extends BaseMapper<TimeSpan> {

}
