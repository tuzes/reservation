package com.tuzes.reservation.biz.common;

/**
 * @Description: 预约状态
 * @author: tuzes
 * @date: 2023年07月11日 19:49
 */
public class ReservationState {

    /**
    ·* 待确定
    ·*/
    public static final Integer WAITED = 10;

    /**
     ·* 已确定
     ·*/
    public static final Integer FINISH = 20;
}
