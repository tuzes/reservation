package com.tuzes.reservation.biz.common;

/**
 * @Description: TODO
 * @author: tuzes
 * @date: Y E A R 年 {YEAR}年YEAR年{MONTH}月09日 21:28
 */
public class UserType {
    //普通用户
    public static final String USER = "USER";

    //管理员
    public static final String ADMIN = "ADMIN";
}
