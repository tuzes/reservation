/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80026 (8.0.26)
 Source Host           : localhost:3306
 Source Schema         : reservation

 Target Server Type    : MySQL
 Target Server Version : 80026 (8.0.26)
 File Encoding         : 65001

 Date: 11/07/2023 22:09:02
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for code_generator_test
-- ----------------------------
DROP TABLE IF EXISTS `code_generator_test`;
CREATE TABLE `code_generator_test`  (
  `id` bigint NULL DEFAULT NULL,
  `value1` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `value2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `value3` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of code_generator_test
-- ----------------------------
INSERT INTO `code_generator_test` VALUES (1, '1', '2', '3');
INSERT INTO `code_generator_test` VALUES (2, '2', '3', '4');

-- ----------------------------
-- Table structure for reservation
-- ----------------------------
DROP TABLE IF EXISTS `reservation`;
CREATE TABLE `reservation`  (
  `id` bigint NOT NULL,
  `user_id` bigint NULL DEFAULT NULL,
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `date` datetime NULL DEFAULT NULL,
  `time_span` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '时间段codeList',
  `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '某次预约唯一标识\r\n即一次预约中包含多个时间段，会有多条记录，但是一次预约code是唯一的',
  `state` int NULL DEFAULT NULL COMMENT '状态',
  `created_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '预约表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of reservation
-- ----------------------------
INSERT INTO `reservation` VALUES (1678767962114154497, 1688915410522, NULL, NULL, NULL, '预约', '2023-07-11 11:15:11', 'a', '7aff906a-4651-43b5-80c3-a3249aee0149', 10, '2023-07-11 22:07:06');
INSERT INTO `reservation` VALUES (1678767962118348801, 1688915410522, NULL, NULL, NULL, '预约', '2023-07-11 11:15:11', 'b', '7aff906a-4651-43b5-80c3-a3249aee0149', 10, '2023-07-11 22:07:06');
INSERT INTO `reservation` VALUES (1678767962122543106, 1688915410522, NULL, NULL, NULL, '预约', '2023-07-11 11:15:11', 'c', '7aff906a-4651-43b5-80c3-a3249aee0149', 10, '2023-07-11 22:07:06');

-- ----------------------------
-- Table structure for test
-- ----------------------------
DROP TABLE IF EXISTS `test`;
CREATE TABLE `test`  (
  `id` bigint NOT NULL,
  `value` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of test
-- ----------------------------
INSERT INTO `test` VALUES (1, '1');
INSERT INTO `test` VALUES (3, '4');

-- ----------------------------
-- Table structure for time_span
-- ----------------------------
DROP TABLE IF EXISTS `time_span`;
CREATE TABLE `time_span`  (
  `id` bigint NOT NULL,
  `time_span` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '时间段',
  `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'code（唯一标识',
  `relation_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '关联时间段codeList（即该此时间段选中后，其他也不能选中的时间段）\r\n',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '时间段表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of time_span
-- ----------------------------
INSERT INTO `time_span` VALUES (1, '9:30-13:30', 'a', '[\"a\",\"b\",\"c\"]');
INSERT INTO `time_span` VALUES (2, '9:30-14:30', 'b', '[\"a\",\"b\",\"c\"]');
INSERT INTO `time_span` VALUES (3, '9:30-15:30', 'c', '[\"a\",\"b\",\"d\"]');
INSERT INTO `time_span` VALUES (4, '14:30-18:30', 'd', '[\"c\",\"d\",\"e\"]');
INSERT INTO `time_span` VALUES (5, '15:30-19:30', 'e', '[\"d\",\"e\"]');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '真实姓名',
  `phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '手机号',
  `company` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '公司名',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP,
  `user_type` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '用户类型：admin  user',
  `account` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '账号',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '密码',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1688915591419 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1688915410522, 'name1', '18533333333', NULL, '2023-07-09 23:10:10', 'ADMIN', '18533333333', NULL);
INSERT INTO `user` VALUES (1688915591418, 'name2', '18533333334', NULL, '2023-07-09 23:13:11', 'ADMIN', '18533333334', NULL);

SET FOREIGN_KEY_CHECKS = 1;
